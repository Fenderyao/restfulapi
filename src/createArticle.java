import java.io.IOException; 
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter; 
import java.util.Date;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
/**
 * Servlet implementation class createArticle
 * fender yao 2016/5/5
 * hao hao ming 2016
 */
@WebServlet("/createArticle")
public class createArticle extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String ENCODING = "UTF-8";
	private static final String TrustToken = "123456";   //Fixed authentication Token
   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public createArticle() {
        super();
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	//	-------------------Init-------------------------------
		request.setCharacterEncoding(ENCODING);
		response.setCharacterEncoding(ENCODING);
	String Token =new String();
	String articleTitle=new String();
	 int articleType=0;
	 int isSlider=0;
	 String articleDesc=new String();
	 String articleUrl=new String();
	 String articleImgUrl=new String(); 
	// System.out.println("request>>"+request.getReader().lines().collect(Collectors.joining(System.lineSeparator())));
	 
	 PrintWriter out = response.getWriter();
	 //--------------------------------------------------------	
	   Token=request.getParameter("Token");
	
	   
     if (Token.equals(TrustToken)){     //  Token Check
    	   // --------------- Pass Value -------------------------
    	   articleTitle=request.getParameter("articleTitle");
    	   articleType=Integer.valueOf(request.getParameter("articleType"));
    	   isSlider=Integer.valueOf(request.getParameter("isSlider"));
    	   articleDesc=request.getParameter("description");
    	   articleUrl=request.getParameter("linkUrl");
    	   articleImgUrl=request.getParameter("imgUrl");
    	   //----------------------------------------------------
    	    MysqlUtil mysqlUtil = new MysqlUtil();
    		Connection connection = mysqlUtil.getConnection();
    		String sql = "INSERT INTO articletable(ArticleTitle,ArticleType,isSlider,description,LinkUrl, ImgUrl) VALUES(?, ?, ?, ?, ?, ?)";
    		PreparedStatement preparedStatement = null;
    	 
    		 
    		try {
    			preparedStatement = connection.prepareStatement(sql);
    			preparedStatement.setString(1, articleTitle);
    			preparedStatement.setInt(2, articleType);
    			preparedStatement.setInt(3, isSlider);
    			preparedStatement.setString(4,articleDesc);
    			preparedStatement.setString(5,articleUrl);
    			preparedStatement.setString(6,articleImgUrl);
    		 	preparedStatement.execute();
    			response.setStatus(200);
    		} catch (SQLException e) {
    			response.sendError(400, "SQL Server Error" );
    			e.printStackTrace();
    		} finally {
    			MysqlUtil.releasePreparedStatement(preparedStatement);
    			MysqlUtil.releaseConnection(connection);
    		}
    		
    		out.println("<html>");
    		out.println("<body>");
    		out.println("<h1>Complete add article</h1>");
    		out.println("</body>");
    		out.println("</html>");	
    	
      }else{
        response.setStatus(500);   // 500 return authentication is fail
   		out.println("<html>");
		out.println("<body>");
		out.println("<h1>Token wrong</h1>");
		out.println("</body>");
		out.println("</html>");
       }
   	out.flush();
	out.close();
   
	}
	
}
