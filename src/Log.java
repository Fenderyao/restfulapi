//package cn.taotaoxi.WechatService.Util;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Log {

	private static final String TAG = "[TTX]";

	private static final boolean PRINT_LOG = true;

	private static final Logger logger = Logger.getLogger(TAG);

	public static void d(String tag, String message) {
		if (PRINT_LOG) {
			logger.log(Level.FINE, mergeMessage(tag, message));
		}
	}

	public static void i(String tag, String message) {
		if (PRINT_LOG) {
			logger.log(Level.INFO, mergeMessage(tag, message));
		}
	}

	public static void w(String tag, String message) {
		if (PRINT_LOG) {
			logger.log(Level.WARNING, mergeMessage(tag, message));
		}
	}

	public static void e(String tag, String message) {
		if (PRINT_LOG) {
			logger.log(Level.SEVERE, mergeMessage(tag, message));
		}
	}

	private static String mergeMessage(String tag, String message) {
		StringBuilder stringBuilder = new StringBuilder();

		stringBuilder.append("[");
		stringBuilder.append(tag);
		stringBuilder.append("]");

		stringBuilder.append(" ");
		stringBuilder.append(message);

		return stringBuilder.toString();
	}
}
